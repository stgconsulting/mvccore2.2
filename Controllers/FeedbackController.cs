﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MvcCoreTwoTwo.Data;
using MvcCoreTwoTwo.Models;
using MvcCoreTwoTwo.Repository;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MvcCoreTwoTwo.Controllers
{
    [Authorize]
    public class FeedbackController : Controller
    {
        private readonly IFeedbackRepository _feedbackRepository;
        public FeedbackController(IFeedbackRepository feedbackRepository)
        {
            _feedbackRepository = feedbackRepository;
        }
        // GET: /<controller>/
 
        public IActionResult CreateFeedback()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CreateFeedback(Feedback feedback)
        {
            if (!ModelState.IsValid)
                return View(feedback);

            _feedbackRepository.AddFeedback(feedback);
            return RedirectToAction("FeedbackCompleted");
        }

        public IActionResult FeedbackCompleted()
        {
            return View();
        }
    }
}
