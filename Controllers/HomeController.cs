﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MvcCoreTwoTwo.Repository;
using MvcCoreTwoTwo.ViewModels;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MvcCoreTwoTwo.Controllers
{
    public class HomeController : Controller
    {
        // GET: /<controller>/
        private readonly IPaintRepository _paintRepository;
        public HomeController(IPaintRepository paintRepository)
        {
            _paintRepository = paintRepository;
        }
        public IActionResult Index()
        {
            var paints = _paintRepository.GetAllPainting().OrderBy(p => p.Name);
            var homeViewModel = new HomwViewModel()
            {
                Title = "Welcome to Shanaz Painting Gallery",
                Paints = paints.ToList()
            };
            
          
            return View(homeViewModel);
        }

        public IActionResult Details(int id)
        {
            var paint = _paintRepository.GetPaintById(id);
            if (paint == null)
                return NotFound();
            return View(paint);
        }
    }
}
