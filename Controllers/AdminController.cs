﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MvcCoreTwoTwo.Models;
using MvcCoreTwoTwo.Repository;

namespace MvcCoreTwoTwo.Controllers
{
    [Authorize(Roles="Admin")]
    public class AdminController : Controller
    {

        private readonly IPaintRepository _paintRepository;
        public AdminController(IPaintRepository paintRepository)
        {
            _paintRepository = paintRepository;
        }
        public IActionResult AddPaint()
        {
            return View();
        }

        [HttpPost]
        public IActionResult AddPaint(Paint newPaint)
        {
            if (!ModelState.IsValid)
            {
                return View(newPaint);
            }
            _paintRepository.AddPaint(newPaint);
            return RedirectToAction("Index","Home");

        }
    }
}