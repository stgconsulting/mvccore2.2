﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MvcCoreTwoTwo.Migrations
{
    public partial class PaintChange : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Details",
                table: "Paints",
                newName: "Description");

            migrationBuilder.AddColumn<decimal>(
                name: "Price",
                table: "Paints",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Price",
                table: "Paints");

            migrationBuilder.RenameColumn(
                name: "Description",
                table: "Paints",
                newName: "Details");
        }
    }
}
