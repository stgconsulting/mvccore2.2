﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MvcCoreTwoTwo.Migrations
{
    public partial class PaintModelChanged : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Title",
                table: "Paints",
                newName: "Name");

            migrationBuilder.AddColumn<string>(
                name: "ImageUrl",
                table: "Paints",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ImageUrl",
                table: "Paints");

            migrationBuilder.RenameColumn(
                name: "Name",
                table: "Paints",
                newName: "Title");
        }
    }
}
