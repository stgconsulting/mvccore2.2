﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MvcCoreTwoTwo.Migrations
{
    public partial class PaintNewChanges : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "Price",
                table: "Paints",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "Price",
                table: "Paints",
                nullable: true,
                oldClrType: typeof(decimal));
        }
    }
}
