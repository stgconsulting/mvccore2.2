﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MvcCoreTwoTwo.Models
{
    public class Feedback
    {
        public int FeedbackId { get; set; }

        [Required]
        [StringLength(100)]
        public string  Name { get; set; }

        [Required]
        [StringLength(100)]
        public string Email { get; set; }

        [Required]
        [StringLength(1000)]
        public string Message { get; set; }
        public bool ContactMe { get; set; }
    }
}
