﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MvcCoreTwoTwo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcCoreTwoTwo.Data
{
    public class PaintDbContext : IdentityDbContext<IdentityUser>
    {
        public PaintDbContext(DbContextOptions<PaintDbContext> options):base(options)
        {

        }
        public DbSet<Paint> Paints { get; set; }
        public DbSet<Feedback> Feedbacks { get; set; }
    }
}
