﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using MvcCoreTwoTwo.Data;
using MvcCoreTwoTwo.Models;
using MvcCoreTwoTwo.Repository;

namespace MvcCoreTwoTwo
{
    public class Startup
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940

        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<PaintDbContext>(options => 
                           options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));

          

            services.AddTransient<IPaintRepository, PaintRepository>();
            services.AddTransient<IFeedbackRepository, FeedbackRepository>();

         
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IServiceProvider serviceProvider)
        {
            app.UseDeveloperExceptionPage(); //this is cause to prevent seeing yellow page
            app.UseStatusCodePages(); //this is for 500, 400, 200 etc
            app.UseStaticFiles(); //this for static file such css, js, image
            app.UseAuthentication();
            
            app.UseMvcWithDefaultRoute(); //this is a default route
            //testing

            //you can use the following route too
            //app.UseMvc(routes => {
            //    routes.MapRoute(
            //            name:"default",
            //            template: "{controller=Home}/{action=Index}/{id?}"
            //        );
            //});

            CreateUserRoles(serviceProvider).Wait();
        }

        private async Task CreateUserRoles(IServiceProvider serviceProvider)
        {
            var roleManager = serviceProvider.GetRequiredService<RoleManager<IdentityRole>>();
            var userManager = serviceProvider.GetRequiredService<UserManager<IdentityUser>>();
            

            IdentityResult roleResult;

            var roleCheck = await roleManager.RoleExistsAsync("Admin");
            
            if(!roleCheck)
            {
                roleResult = await roleManager.CreateAsync(new IdentityRole("Admin"));   
            }

            IdentityUser user = await userManager.FindByEmailAsync("slavasanie@yahoo.com");
            await userManager.AddToRoleAsync(user, "Admin");

        }
    }
}
