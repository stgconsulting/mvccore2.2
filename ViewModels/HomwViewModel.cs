﻿using MvcCoreTwoTwo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcCoreTwoTwo.ViewModels
{
    public class HomwViewModel
    {
        public string Title { get; set; }
        public List<Paint> Paints { get; set; }
    }
}
