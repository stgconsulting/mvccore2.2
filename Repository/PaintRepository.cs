﻿using MvcCoreTwoTwo.Data;
using MvcCoreTwoTwo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcCoreTwoTwo.Repository
{
    public class PaintRepository : IPaintRepository
    {
        private readonly PaintDbContext _paintDbContext;
        public PaintRepository(PaintDbContext paintDbContext)
        {
            _paintDbContext = paintDbContext;
        }

        public Paint GetPaintById(int paintId)
        {
            return _paintDbContext.Paints.FirstOrDefault(p => p.PaintId == paintId);
        }

        public IEnumerable<Paint> GetAllPainting()
        {
            return _paintDbContext.Paints;
        }

        public void AddPaint(Paint newPaint)
        {
            _paintDbContext.Paints.Add(newPaint);
            _paintDbContext.SaveChanges();

        }
    }
}
