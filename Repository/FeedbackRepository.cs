﻿using MvcCoreTwoTwo.Data;
using MvcCoreTwoTwo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcCoreTwoTwo.Repository
{
    public class FeedbackRepository : IFeedbackRepository
    {
        private readonly PaintDbContext _paintDbContext;
        public FeedbackRepository(PaintDbContext paintDbContext)
        {
            _paintDbContext = paintDbContext;
        }

        public void AddFeedback(Feedback feedback)
        {
            _paintDbContext.Feedbacks.Add(feedback);
            _paintDbContext.SaveChanges();  //persist data to database
        }
    }
}
