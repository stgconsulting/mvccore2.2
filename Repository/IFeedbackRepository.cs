﻿using MvcCoreTwoTwo.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcCoreTwoTwo.Repository
{
    public interface IFeedbackRepository
    {
        void AddFeedback(Feedback feedback);
    }
}
